package com.example.miranpc.maptesting.ServicesJobs;


import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.miranpc.maptesting.AppExecutors;
import com.example.miranpc.maptesting.DB.MapDataDB;
import com.example.miranpc.maptesting.DB.MapDataModel;

import java.util.Calendar;

public class MapService extends IntentService {
    Calendar calendar= Calendar.getInstance();
    MapDataDB mapDataDB;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public MapService() {
        super("name");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String action= intent.getAction();

        mapDataDB =MapDataDB.getInstance(getApplication());

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mapDataDB.mapDao().insertDataToDB(new MapDataModel(calendar.getTime().toString(), intent.getDoubleExtra("lat",0), intent.getDoubleExtra("lng",0)));
            }
        });

    }
}
