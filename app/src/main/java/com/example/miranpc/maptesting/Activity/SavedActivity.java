package com.example.miranpc.maptesting.Activity;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.miranpc.maptesting.DB.MapDataModel;
import com.example.miranpc.maptesting.DB.MapViewModel;
import com.example.miranpc.maptesting.MapDataAdapter;
import com.example.miranpc.maptesting.R;

import java.util.List;

public class SavedActivity extends AppCompatActivity implements MapDataAdapter.OnItemClickListener {

    RecyclerView recyclerView;
    MapDataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        recyclerView = findViewById(R.id.recycler);

        setUpRecycler();


        MapViewModel mapViewModel = new MapViewModel(getApplication());
        mapViewModel.getMapEntityLiveData().observe(this, new Observer<List<MapDataModel>>() {
            @Override
            public void onChanged(@Nullable List<MapDataModel> mapDataModels) {
                adapter.addData(mapDataModels);
                Toast.makeText(SavedActivity.this, "size" + adapter.getItemCount(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void setUpRecycler() {
        adapter = new MapDataAdapter(this, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClicked(int pos) {
    }
}
